package org.example.pom;

import com.epam.reportportal.annotations.Step;
import com.smiling.annotations.components.Page;
import com.smiling.structure.BasePage;
import lombok.extern.log4j.Log4j2;
import org.example.common.Properties;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$x;

@Page
@Log4j2
public class HomePage extends BasePage<HomePage> {
    public HomePage(Properties properties) {
        super(properties.getHomePageUrl(), By.xpath("//body"));
    }

    @Override
    public HomePage openPage() {
        super.openPage().waitUntilPageWillOpen();
        return this;
    }

    @Step("Filling search input by text '{text}'")
    public HomePage fillSearchInputByText(String text) {
        $x("//input[@title]").setValue(text);
        log.info("Search input filled by text '{}'", text);
        return this;
    }

    @Step("Clicking on 'search' button")
    public void clickOnSearchButton() {
        $x("(//input[@type='submit'])[1]").click();
        log.info("Clicked on 'search' button");
    }
}
