package org.example.common;

import org.aeonbits.owner.Config;

@Config.Sources("system:properties")
public interface Properties extends Config {

    @Key("homepage.url")
    String getHomePageUrl();
}
