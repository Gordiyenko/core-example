package org.example.common;

import com.smiling.exceptions.TestRetryException;
import com.smiling.utils.restassured.RestAssuredWrapper;
import io.restassured.http.Cookie;
import io.restassured.http.Cookies;
import io.restassured.response.ExtractableResponse;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractSearchRestService {
    @Getter
    private String domain;
    @Getter
    private String html;
    @Getter
    private String json;
    @Getter
    private Cookies cookies = new Cookies(new ArrayList<>());
    @Getter
    private ExtractableResponse response;

    public AbstractSearchRestService() {

    }

    public AbstractSearchRestService(String domain) {
        this.domain = domain;
    }

    public AbstractSearchRestService(ExtractableResponse response, String domain) {
        this.html = response.body().asString();
        this.cookies = response.detailedCookies();
        this.response = response;
        this.domain = domain;
        this.json = extractJson(html);
    }

    public AbstractSearchRestService(ExtractableResponse response, Cookies cookies, String domain) {
        this.html = response.body().asString();
        this.cookies = updateCookies(response, cookies);
        this.domain = domain;
        this.response = response;
        this.json = extractJson(html);
    }

    protected ExtractableResponse doGetRequest(String request, String onFailMessage) {
        try {
            return RestAssuredWrapper.doRequest().expect()
                    .statusCode(200)
                    .onFailMessage(onFailMessage)
                    .request()
                    .get(request)
                    .then()
                    .extract();
        } catch (AssertionError e) {
            throw new TestRetryException(e.getMessage());
        }
    }

    protected ExtractableResponse doGetRequestWithQueryParam(
            String request, String onFailMessage, String paramKey, List<String> paramValues
    ) {
        try {
            return RestAssuredWrapper.doRequest().expect()
                    .statusCode(200)
                    .onFailMessage(onFailMessage)
                    .given()
                    .queryParams(paramKey, paramValues)
                    .request()
                    .get(request)
                    .then()
                    .extract();
        } catch (AssertionError e) {
            throw new TestRetryException(e.getMessage());
        }
    }

    protected ExtractableResponse doGetRequestWithCookies(String request, String onFailMessage, Cookies cookies) {
        try {
            return RestAssuredWrapper.doRequest().expect()
                    .statusCode(200)
                    .onFailMessage(onFailMessage)
                    .request()
                    .cookies(cookies)
                    .get(request)
                    .then()
                    .extract();
        } catch (AssertionError e) {
            throw new TestRetryException(e.getMessage());
        }
    }

    protected ExtractableResponse postRequestWithCookies(
            String request, Map<String, String> params, String onFailMessage, Cookies cookies
    ) {
        try {
            return RestAssuredWrapper.doRequest().expect()
                    .statusCode(200)
                    .onFailMessage(onFailMessage)
                    .given()
                    .params(params)
                    .request()
                    .cookies(cookies)
                    .post(request)
                    .then()
                    .extract();
        } catch (AssertionError e) {
            throw new TestRetryException(e.getMessage());
        }
    }

    protected abstract String extractJson(String html);

    private Cookies updateCookies(ExtractableResponse response, Cookies previousCookies) {
        Map<String, String> cookies = new HashMap<>();
        previousCookies.asList().forEach(prevCookies -> cookies.put(prevCookies.getName(), prevCookies.getValue()));
        response.detailedCookies().asList().forEach(respCookie -> cookies.put(respCookie.getName(), respCookie.getValue()));

        this.cookies.asList().forEach(cookie -> cookies.put(cookie.getName(), cookie.getValue()));

        List<Cookie> result =
            cookies.entrySet().stream().map(cookie ->
                new Cookie.Builder(cookie.getKey(), cookie.getValue()).build()).collect(Collectors.toList());

        return new Cookies(result);
    }

    protected boolean isAllGone(ExtractableResponse response) {
        if (response.body().asString().contains("All gone!")) {
            return true;
        }
        return false;
    }
}
