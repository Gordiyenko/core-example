package org.example.common;


import com.smiling.testng.executors.AbstractUITestsExecutor;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(classes = {ContextConfigurator.class})
public abstract class BaseTest extends AbstractUITestsExecutor {

}
