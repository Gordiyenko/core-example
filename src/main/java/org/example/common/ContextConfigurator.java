package org.example.common;

import com.smiling.annotations.components.GeneralComponent;
import com.smiling.annotations.components.ModalComponent;
import com.smiling.annotations.components.Page;
import com.smiling.annotations.components.PageComponent;
import org.aeonbits.owner.ConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(
        basePackages = {
                "org.example"
        },
        includeFilters = {
                @ComponentScan.Filter(Page.class),
                @ComponentScan.Filter(GeneralComponent.class),
                @ComponentScan.Filter(PageComponent.class),
                @ComponentScan.Filter(ModalComponent.class)
        }
)
public class ContextConfigurator {

        @Bean
        public Properties getProperties() {
                return ConfigFactory.create(Properties.class);
        }

}
