package org.example.rest;

import com.epam.reportportal.annotations.Step;
import io.restassured.http.Cookies;
import io.restassured.response.ExtractableResponse;
import lombok.extern.log4j.Log4j2;
import org.example.common.AbstractSearchRestService;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class SimpleWeatherRestService extends AbstractSearchRestService {

    public SimpleWeatherRestService() {

    }
    public SimpleWeatherRestService(String domain) {
        super(domain);
    }

    public SimpleWeatherRestService(ExtractableResponse response, String domain) {
        super(response, domain);
    }

    public SimpleWeatherRestService(ExtractableResponse response, Cookies cookies, String domain) {
        super(response, cookies, domain);
    }

    @Step("Getting weather data")
    public void getWeatherRequest() {
        doGetRequest("https://api.openweathermap.org/data/2.5/weather?zip=95050," +
                "us&appid=7e34aa1b1b97fe8e6e0233e91782bfb8&units=imperial",
                "Error occurred on weather request execution");
        log.debug("Successfully sent 'GET weather request'");
    }

    @Override
    protected String extractJson(String html) {
        return null;
    }
}
