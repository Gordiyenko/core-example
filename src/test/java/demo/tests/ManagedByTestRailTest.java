package demo.tests;

import com.smiling.annotations.tetsrail.TestRailData;
import org.example.common.BaseTest;
import org.testng.annotations.Test;

public class ManagedByTestRailTest extends BaseTest {

    @TestRailData(caseId = 1)
    @Test
    public void testRailTest1() {
        System.err.println("TestRail test 1  - executed");
    }

    @TestRailData(caseId = 2)
    @Test
    public void testRailTest2() {
        System.err.println("TestRail test 2  - executed");
    }

    @TestRailData(caseId = 3)
    @Test
    public void testRailTest3() {
        System.err.println("TestRail test 3  - executed");
    }

    @TestRailData(caseId = 4)
    @Test
    public void testRailTest4() {
        System.err.println("TestRail test 4  - executed");
    }

    @Test
    public void testRailTest5() {
        System.err.println("TestRail test 5  - executed");
    }
}
