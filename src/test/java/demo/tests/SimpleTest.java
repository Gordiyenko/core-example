package demo.tests;

import lombok.extern.log4j.Log4j2;
import org.example.common.BaseTest;
import org.example.pom.HomePage;
import org.example.rest.SimpleWeatherRestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

@Log4j2
public class SimpleTest extends BaseTest {

    @Autowired private HomePage homePage;
    @Autowired private SimpleWeatherRestService restService;


    @Test
    public void justSimpleExecutionTest() {
        homePage
                .openPage()
                .fillSearchInputByText("What ReportPortal is?")
                .clickOnSearchButton();
    }

    @Test
    public void simpleRestTest() {
        restService.getWeatherRequest();
    }
}
